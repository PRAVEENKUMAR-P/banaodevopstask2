Write a unit test that connects to atg.world website. On proper loading of the website, the unit test should pass. If the website doesn’t load, the unit test should fail. Mention the steps as log or print statements happening at all times.

To Slove the above problem follow given steps

Part 1: Unit Test for atg.world Website
Write Python Selenium Test:

Use the previous Python code provided for testing the atg.world website.
Ensure it has logging or print statements for steps and results.
Version Control:

Store your Python test code in a version control system like Git (GitHub, GitLab, Bitbucket, etc.).
Install Jenkins:

Set up Jenkins on your server following the instructions provided on the official Jenkins website.
Configure Jenkins properly to run builds and tests.
Install Necessary Plugins:

Install the necessary Jenkins plugins to execute Python scripts or Selenium tests. For Python-related plugins, you might need plugins like Python Plugin, Git Plugin, Selenium Plugin, etc.
Create a Jenkins Job:

Create a new Jenkins job for your test.
Configure the job to pull the code from the version control system where your test code resides (e.g., GitHub).
Define the build steps to execute your Python test script.
Ensure the output of the script (pass/fail results) is properly captured.
Configure Test Execution:

Set up Jenkins to execute your Python test script that connects to atg.world.
Ensure proper logging or print statements are captured by Jenkins.
Schedule or Trigger the Test:

Set up the job to run on a schedule or trigger it manually to test connectivity to the atg.world website.
Jenkins will execute the Python test script and report pass/fail results based on the website's loading status.
Part 2: Domain Configuration
Domain Setup:

Purchase or use a free domain provider to set up your main domain (www.domainname.com) and a subdomain (jenkins.domainname.com).
Point these domains to your server's IP address using DNS configurations.
Server Configuration:

Configure your server (DNS settings, web server settings if necessary) to handle requests to these domains properly.
Ensure that the server can serve both www and jenkins subdomain requests.
SSL Certificates (Optional but Recommended):

Secure your domains with SSL certificates to enable HTTPS connections. Services like Let's Encrypt offer free SSL certificates.
Verification:

Test both domains (www and jenkins subdomain) to ensure they are properly set up and accessible from the internet.




Create a Jenkins Job:
Log in to Jenkins:

Open your web browser and navigate to your Jenkins dashboard by entering the URL for your Jenkins server.
Create a New Jenkins Job:

Click on "New Item" or "Create New Job" on the Jenkins dashboard.
Enter a name for your job (e.g., "ATG World Website Test") and select the type of job (freestyle project or pipeline).
Configure Source Code Management:

Under the "Source Code Management" section, select the version control system where your test code resides (e.g., Git).
Provide the repository URL and any necessary credentials to access the repository.
Define Build Steps:

Scroll down to the "Build" section.
Click on "Add build step" or "Add build step/Execute shell" depending on your job type.
Add commands to execute your Python test script. For example:

        | Bash Script |

# Navigate to the directory containing your Python test script
cd path/to/your/test/script/directory

# Run the Python test script
python your_test_script.py


      |  OR  |

    Pipeline Script


pipeline {
    agent any

    stages {
        stage('Run Python Test') {
            steps {
                script {
                    // Change directory to where your Python script resides
                    dir('path_to_your_python_script_directory') {
                        sh 'python test_website_connection.py' // Execute the Python script
                    }
                }
            }
        }
    }
}



Ensure the script handles capturing pass/fail results appropriately. This could be through exit codes or specific output messages.
Capture Output:

Jenkins automatically captures the console output of the build steps.
Ensure that your Python test script produces clear and identifiable messages for pass/fail results, errors, or any other relevant information.
Save the Job Configuration:

Click "Save" or "Apply" to save the job configuration in Jenkins.



Install Python packages such as requests and unittest on an Ubuntu server, you can use pip, the Python package manager. Here are the steps:

Update package lists: First, update the package lists to ensure you have the latest versions available:

# update the package
sudo apt update

# Install Python and pip
sudo apt install python3-pip

# Install Python packages
pip3 install requests
